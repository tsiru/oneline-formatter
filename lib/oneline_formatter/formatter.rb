require 'rspec'
require "rspec/core/formatters/base_text_formatter"
require "rspec/core/formatters/console_codes"

module OnelineFormatter
  class Formatter < RSpec::Core::Formatters::BaseTextFormatter
    RSpec::Core::Formatters.register self,
      :example_passed, :example_pending, :example_failed, :start_dump, :dump_summary

    def initialize(*args)
      super
      @success_count = 0
      @pending_count = 0
      @failed_count  = 0
      @total_count   = 0
    end

    def example_passed(_example)
      # super
      @success_count += 1
      @total_count   += 1
      display_counter
    end

    def example_pending(_example)
      # super
      @pending_count += 1
      @total_count   += 1
      display_counter
    end

    def example_failed(_example)
      # super
      @failed_count += 1
      @total_count  += 1
      display_counter
    end

    def start_dump(*_args)
      new_line
    end

    def dump_summary(*_args)
      new_line
      super
    end

    private

    def new_line
      output.puts
      output.puts
    end

    def display_counter
      output.print "\r"
      output.print "passed: #{success_color @success_count}, "
      output.print "pending: #{pending_color @pending_count}, "
      output.print "failed: #{failure_color @failed_count}, "
      output.print "total: #{@total_count} "
    end

    def success_color(message)
      RSpec::Core::Formatters::ConsoleCodes.wrap(message, :success)
    end

    def pending_color(message)
      RSpec::Core::Formatters::ConsoleCodes.wrap(message, :pending)
    end

    def failure_color(message)
      RSpec::Core::Formatters::ConsoleCodes.wrap(message, :failure)
    end

  end
end
