$LOAD_PATH << File.join(File.dirname(__FILE__), 'lib')

require 'oneline_formatter/version'

Gem::Specification.new do |spec|
  spec.name          = "oneline-formatter"
  spec.version       = OnelineFormatter::VERSION
  spec.authors       = ["Bartłomiej Rapacz"]
  spec.email         = ["bartłomiej.rapacz@gmail.com"]
  spec.description   = %q{Displays simple summary in single line instead long line of chars}
  spec.summary       = spec.description
  spec.homepage      = ""
  spec.license       = "MIT"


  spec.files         = Dir.glob("lib/**/*")
  spec.test_files    = Dir.glob("{test,spec,features}/**/*")
  spec.executables   = Dir.glob("bin/*").map{ |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'rspec', '>= 3.0'

  spec.add_development_dependency "rake", "~> 10.0"
end
