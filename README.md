# Rspec::Oneline::Formatter

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'oneline-formatter'
```

and to .rspec file:
```
--format OnelineFormatter::Formatter
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install oneline-formatter

## Usage

Just execute:

```
rspec
```
